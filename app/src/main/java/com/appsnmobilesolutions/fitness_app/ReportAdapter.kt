package com.appsnmobilesolutions.fitness_app

import android.content.Context
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView


public class ReportAdapter(val reportModelList: ArrayList<ReportModel>, private val context: Context?) : RecyclerView.Adapter<ReportAdapter.ReportItemHolder>() {

    init {
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReportAdapter.ReportItemHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.report_card_view, parent, false)
        val viewHolder = ReportAdapter.ReportItemHolder(v)
        return viewHolder
    }

    override fun onBindViewHolder(holder: ReportAdapter.ReportItemHolder, position: Int) {


        val reportModel = reportModelList[position]


        holder.txtTimeUsed.text = reportModel.time_used
        holder.txtDistance.text = reportModel.distance_covered

    }

    override fun getItemCount(): Int {
        return reportModelList.size
    }

    class ReportItemHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtTimeUsed: TextView
        var txtDistance: TextView
        var cardview: CardView
        var status_color: ImageView

        init {
            txtTimeUsed = itemView.findViewById(R.id.txtTimeUsed)
            status_color = itemView.findViewById(R.id.status_color)
            txtDistance = itemView.findViewById(R.id.txtDistance)
            cardview = itemView.findViewById(R.id.cardview1)
        }
    }
}