package com.appsnmobilesolutions.fitness_app

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import java.util.*

class RecordsActivity : AppCompatActivity() {

    private lateinit var recyclerReport: RecyclerView
    lateinit var reportAdapter: ReportAdapter
    private lateinit var reportModelList: ArrayList<ReportModel>
    private lateinit var textview: TextView
    private lateinit var fabNext: FloatingActionButton

    private var db: DatabaseHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_records)

        reportModelList = java.util.ArrayList()

        db = DatabaseHelper(this)

        val permissions = arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION)
        ActivityCompat.requestPermissions(this, permissions,0)

        recyclerReport = findViewById(R.id.recyclerRecords)
        fabNext = findViewById(R.id.fabNext)
        textview = findViewById(R.id.textView)

        reportAdapter = ReportAdapter(reportModelList, this)
        reportAdapter.notifyDataSetChanged()

        recyclerReport.setHasFixedSize(true)
        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL
        recyclerReport.layoutManager = llm

        recyclerReport.adapter = reportAdapter


        fabNext.setOnClickListener {
            val intent = Intent(this,MainActivity::class.java)
            startActivity(intent)
        }

        loadRecords()
    }

    private fun loadRecords() {
        reportModelList.clear()
        val cursor = db!!.allRecords
        if (cursor.moveToFirst()) {
            do {
                val n = ReportModel(
                    cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_TIME_USED)),
                    cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_DISTANCE_COVERED))
                )
                reportModelList.add(n)
                //refreshList();
            } while (cursor.moveToNext())
        }
        refreshList()

        Log.d("CUR", cursor.toString())


    }

    private fun refreshList() {
        reportAdapter.notifyDataSetChanged()
    }

    override fun onStart() {
        super.onStart()
        loadRecords()
    }

    override fun onResume() {
        super.onResume()
        loadRecords()
    }
}
