package com.appsnmobilesolutions.fitness_app

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.os.SystemClock
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Chronometer
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.LatLng
import java.math.RoundingMode
import java.text.DecimalFormat
import java.util.*


class MainActivity : AppCompatActivity(),
    GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
    com.google.android.gms.location.LocationListener,
    View.OnClickListener {


    //private lateinit var static final String TAG = "MainActivity";
    private lateinit var txtMeters: TextView
    private lateinit var txtTimer: TextView
    private lateinit var imgStart: ImageView
    private lateinit var imgStop: ImageView
    private lateinit var mGoogleApiClient: GoogleApiClient
    private lateinit var mLocation: Location
    private lateinit var mLocationManager: LocationManager
    private lateinit var chronometer: Chronometer
    private var running: Boolean = false

    lateinit var start_latlng: LatLng
    private lateinit var stop_latLng: LatLng

    lateinit var reportModelList: ArrayList<ReportModel>

    lateinit var reportModel: ReportModel

    private lateinit var mLocationRequest: LocationRequest
    private val listener: com.google.android.gms.location.LocationListener? = null

    private var reportAdapter: ReportAdapter? = null

    private var UPDATE_INTERVAL: Long = 2 * 1000  /* 10 secs */
    private var FASTEST_INTERVAL: Long = 2000 /* 2 sec */
    private var distance: Double = 0.0
    private var db: DatabaseHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val permissions = arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION)
        ActivityCompat.requestPermissions(this, permissions,0)

        txtMeters = findViewById(R.id.txtMeters)
        txtTimer = findViewById(R.id.txtTimer)
        imgStart = findViewById(R.id.imgStart)
        imgStop = findViewById(R.id.imgStop)

        reportModelList = java.util.ArrayList()

        chronometer = findViewById(R.id.chronometer)

        db = DatabaseHelper(this)
        reportAdapter = ReportAdapter(reportModelList, this)

        mGoogleApiClient = GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build()

        mLocationManager = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager

        imgStart.setOnClickListener(this)
        imgStop.setOnClickListener(this)

        checkLocation() //check whether location service is enable or not in your  phone
    }

    override fun onConnected(bundle: Bundle?) {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) !== PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) !== PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }

        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient)
        startLocationUpdates()



        if (mLocation == null) {
            startLocationUpdates()
        }
        if (mLocation != null) {

            // mLatitudeTextView.setText(String.valueOf(mLocation.getLatitude()));
            //mLongitudeTextView.setText(String.valueOf(mLocation.getLongitude()));
        } else {
            Toast.makeText(this, "Location not Detected", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onClick(v: View?) {

        val df = DecimalFormat("#.##")
        df.roundingMode = RoundingMode.CEILING

        if (v != null) {
            when (v.id) {
                R.id.imgStart -> {
                    if (!running) {
                        chronometer.base = SystemClock.elapsedRealtime()
                        chronometer.start()
                        running = true
                        imgStop.visibility = View.VISIBLE
                        imgStart.visibility = View.GONE


                        chronometer.visibility = View.VISIBLE
                        txtMeters.visibility = View.GONE
                    }
                }

                R.id.imgStop -> {
                    if (running) {
                        chronometer.stop()
                        running = false
                        imgStart.visibility = View.VISIBLE
                        imgStop.visibility = View.GONE


                        chronometer.visibility = View.VISIBLE
                        txtMeters.visibility = View.VISIBLE

                        txtMeters.text = "${df.format(distance)}\n Meters"
                        val dbTimer = "${df.format(distance)} Meters"


                        saveNameToLocalStorage(chronometer.text.toString(), dbTimer)

                        Toast.makeText(this, "Added", Toast.LENGTH_LONG).show()

                    }
                }
            }
        }
    }


    private fun saveNameToLocalStorage(
        time_used: String,
        distance_covered: String
    ) {
        db!!.addRecord(time_used, distance_covered)
        val n = ReportModel(time_used, distance_covered)
        reportModelList.add(n)
        refreshList()
    }

    private fun refreshList() {
        reportAdapter!!.notifyDataSetChanged()
    }

    override fun onConnectionSuspended(i: Int) {
        Log.i("MainActivity", "Connection Suspended")
        mGoogleApiClient.connect()
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        Log.i("MainActivity", "Connection failed. Error: " + connectionResult.errorCode)
    }

    override fun onStart() {
        super.onStart()
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect()
        }
    }

    override fun onStop() {
        super.onStop()
        if (mGoogleApiClient.isConnected) {
            mGoogleApiClient.disconnect()
        }
    }

    protected fun startLocationUpdates() {
        // Create the location request
        mLocationRequest = LocationRequest.create()
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setInterval(UPDATE_INTERVAL)
            .setFastestInterval(FASTEST_INTERVAL)
        // Request location updates
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) !== PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) !== PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    android.Manifest.permission.ACCESS_COARSE_LOCATION,
                    android.Manifest.permission.ACCESS_FINE_LOCATION
                ),
                1
            )
            return
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(
            mGoogleApiClient,
            mLocationRequest, this
        )
        Log.d("reque", "--->>>>")
    }


    override fun onLocationChanged(location: Location) {


        val msg = "Updated Location: " +
                java.lang.Double.toString(location.latitude) + "," +
                java.lang.Double.toString(location.longitude)

        // You can now create a LatLng Object for use with maps
        if (!running) {
            start_latlng = LatLng(location.latitude, location.longitude)


        } else if (running) {
            stop_latLng = LatLng(location.latitude, location.longitude)

            //distance = SphericalUtil.computeDistanceBetween(start_latlng, stop_latLng)
            distance = meterDistanceBetweenPoints(
                start_latlng.latitude,
                start_latlng.longitude,
                stop_latLng.latitude,
                stop_latLng.longitude
            )
        }


        // Toast.makeText(this, "$latLng2", Toast.LENGTH_LONG).show()

    }

    private fun meterDistanceBetweenPoints(lat_a: Double, lng_a: Double, lat_b: Double, lng_b: Double): Double {
        val pk = (180f / Math.PI).toFloat()

        val a1 = lat_a / pk
        val a2 = lng_a / pk
        val b1 = lat_b / pk
        val b2 = lng_b / pk

        val t1 = Math.cos(a1.toDouble()) * Math.cos(a2.toDouble()) * Math.cos(b1.toDouble()) * Math.cos(b2.toDouble())
        val t2 = Math.cos(a1.toDouble()) * Math.sin(a2.toDouble()) * Math.cos(b1.toDouble()) * Math.sin(b2.toDouble())
        val t3 = Math.sin(a1.toDouble()) * Math.sin(b1.toDouble())
        val tt = Math.acos(t1 + t2 + t3)

        return 6366000 * tt
    }


    private fun checkLocation(): Boolean {
        if (!isLocationEnabled())
            showAlert()
        return isLocationEnabled()
    }

    private fun showAlert() {
        val dialog = AlertDialog.Builder(this)
        dialog.setTitle("Enable Location")
            .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " + "use this app")
            .setPositiveButton("Location Settings") { paramDialogInterface, paramInt ->
                val myIntent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(myIntent)
            }
            .setNegativeButton("Cancel") { paramDialogInterface, paramInt -> }
        dialog.show()
    }

    private fun isLocationEnabled(): Boolean {

        return mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || mLocationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }
}
