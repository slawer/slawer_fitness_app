package com.appsnmobilesolutions.fitness_app

data class ReportModel(
    val time_used: String,
    val distance_covered: String
)