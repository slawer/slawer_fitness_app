package com.appsnmobilesolutions.fitness_app

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

/**
 * Created by Belal on 1/27/2017.
 */
class DatabaseHelper//Constructor
    (context: Context) : SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION) {

    /*
    * this method will give us all the name stored in sqlite
    * */
    val allRecords: Cursor
        get() {
            val db = this.readableDatabase
            val sql = "SELECT * FROM $TABLE_NAME ORDER BY $COLUMN_ID ASC;"
            return db.rawQuery(sql, null)
        }


    //creating the database
    override fun onCreate(db: SQLiteDatabase) {
        val sql = ("CREATE TABLE " + TABLE_NAME
                + "(" + COLUMN_ID +
                " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COLUMN_TIME_USED + " VARCHAR, "
                + COLUMN_DISTANCE_COVERED + " VARCHAR);"
                )
        db.execSQL(sql)
    }

    //upgrading the database
    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        val sql = "DROP TABLE IF EXISTS Persons"
        db.execSQL(sql)
        onCreate(db)
    }

    fun addRecord(
        time_used: String,
        distance_covered: String
    ): Boolean {
        val db = this.writableDatabase
        val contentValues = ContentValues()

        contentValues.put(COLUMN_TIME_USED, time_used)
        contentValues.put(COLUMN_DISTANCE_COVERED, distance_covered)



        db.insert(TABLE_NAME, null, contentValues)
        db.close()
        return true
    }

    companion object {

        //Constants for Database name, table name, and column names
        val DB_NAME = "RecordsDB"
        val TABLE_NAME = "records"
        val COLUMN_ID = "id"
        val COLUMN_TIME_USED = "time_used"
        val COLUMN_DISTANCE_COVERED = "distance_covered"

        //database version
        private val DB_VERSION = 1
    }
}
